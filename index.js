
'use strict'

// const etag = require('etag')
const restify = require('restify')
const server = restify.createServer()

const restifyBodyParser = require('restify-plugins').bodyParser
server.use(restifyBodyParser())
const restifyQueryParser = require('restify-plugins').queryParser
server.use(restifyQueryParser())
const restifyAuthParser = require('restify-plugins').authorizationParser
server.use(restifyAuthParser())

const notes = require('./notes.js')
const defaultPort = 8080

const status = {
	'OK': 200,
	'badRequest': 400
}

server.get('/notes', async(req, res) => {
	try {
		const allNotes = notes.getAllNotes()
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')
		res.send(status.OK, {notes: allNotes})
	} catch(err) {
		res.send(status.badRequest, {error: err.message})
	}
})

server.post('/notes', async(req, res) => {
	try {
		//TODO
	} catch(err) {
		//TODO
	}
})

console.log(`NODE_ENV: ${process.env['NODE_ENV']}`)
const port = process.env.PORT || defaultPort

server.listen(port, err => {
	if (err) {
		console.error(err)
	} else {
		console.log(`App is ready on port ${port}`)
	}
})
