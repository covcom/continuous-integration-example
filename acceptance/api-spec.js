
'use strict'

const frisby = require('frisby')
const status = require('../status-codes.json')
const url = 'https://notes-api-test.herokuapp.com'
console.log(url)
console.log(status.ok)

it('should be a teapot', done => {
	frisby.get('http://httpbin.org/status/200')
		.expect('status', status.ok)
		.done(done)
});

it ('should return a status of 200', done => {
	frisby
		.get(`${url}/notes`)
		.expect('status', status.ok)
		.done(done)
})
